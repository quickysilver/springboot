package trakkr.handler

import org.springframework.web.bind.annotation.*
import trakkr.entities.User
import trakkr.service.UserService

@RestController
class UserHandler(
    private val userService: UserService
) {
    @PostMapping("api/user/")
    fun createUser(@RequestBody user: User): User = userService.createUser(user)

    @GetMapping("/api/user/{id}/")
    fun getUserById(@PathVariable("id") id: Long): User = userService.getById(id)

    @PutMapping("/api/user/{id}/")
    fun updateUSer(@RequestBody body: User, @PathVariable id: Long): User =
        userService.updateUser(body, id)
    @DeleteMapping("/api/user/{id}/")
    fun deleteUser(@PathVariable("id") id: Long) = userService.deleteUser(id)
    @GetMapping("/api/users/")
    fun getAllUsers(): List<User> = userService.getAllUsers()
}