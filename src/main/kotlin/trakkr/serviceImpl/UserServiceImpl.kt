package trakkr.serviceImpl

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import trakkr.entities.User
import trakkr.repositories.UserRepository
import trakkr.service.UserService
import java.util.*
@Service
class UserServiceImpl (
    private val repository: UserRepository
    ): UserService {
    override fun createUser(user: User): User {
        // save the user using the UserRepository
        return repository.save(user)
    }

    override fun getById(id: Long): User {
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist!")
        }
        return user
    }

    override fun updateUser(body: User, id: Long): User {
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist!")
        }
        return repository.save(user.copy(
            contactDetails = body.contactDetails,
            contactType = body.contactType
        ))
    }


    override fun deleteUser(id: Long) {
        val user = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist!")
        }
        repository.delete(user)
    }

    override fun getAllUsers(): List<User> {
        val users = repository.findAll()
        return users.toList()
    }

}