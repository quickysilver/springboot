package trakkr.entities

import javax.persistence.*

@Entity(name = "User")
@Table(name= "users")
data class User (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @Column(
        nullable = false,
        updatable = true,
        name = "contactDetails"
    )
    var contactDetails: String,

    @Column(
        nullable = false,
        updatable = true,
        name= "contactType"
    )
    var contactType: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "isPrimary"
    )
    var isPrimary: Boolean = true,
)
