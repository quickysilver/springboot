package trakkr.repositories

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import trakkr.entities.User


@Repository
interface UserRepository : CrudRepository<User, Long>